# REST API service for calculating the cost of insurance depending on the type of cargo and the declared value (OS).

## How to install
To work, you need Python version 3.10 or higher and installed containerization software - [Docker](https://docs.docker.com/engine/install/).

Setting environment variables
1. Copy the .env.dist file to .env
2. Complete the .env file. Example:
```yaml
DB_URL="postgres://postgres:postgres@db:5432/insurance"
```

### How to set up a web server on docker
 go to the root directory and run the script
```shell
 docker-compose up --build
```


The service will be available by url: http://127.0.0.1:8000/

#### There is a route to upload a file with tariffs: http://127.0.0.1:8000/insurance/upload/file/
but for that you have to create json file with body like this
```json
   {
     "2020-06-01": [
        {
          "cargo_type": "Glass",
          "rate": 0.04
        },
        {
          "cargo_type": "Other",
          "rate": 0.01
        }
      ],
    
    "2020-07-01": [
        {
          "cargo_type": "Glass",
          "rate": 0.035
        },
        {
          "cargo_type": "Other",
          "rate": 0.015
        }
      ]
    }
```
#### You can also send a simple request to: http://127.0.0.1:8000/insurance/tariff/create/multy
request body example
```json
{
  "data":
    {
     "2020-06-01": [
        {
          "cargo_type": "Glass",
          "rate": 0.04
        },
        {
          "cargo_type": "Other",
          "rate": 0.01
        }
      ],
    
    "2020-07-01": [
        {
          "cargo_type": "Glass",
          "rate": 0.035
        },
        {
          "cargo_type": "Other",
          "rate": 0.015
        }
      ]
    }
}
```

### Calculate insurance cost: http://127.0.0.1:8000/insurance/tariff/create/multyinsurance/calculate/
request body
```json
{
  "cargo_type": "Glass",
  "declared_value": 2,
  "date": "2020-07-01"
}
```

### In general, you can see all available enpoints here: http://127.0.0.1:8000/docs