from fastapi import FastAPI
from src.insurance.router import router as insurance_router
from src.database import connect_db
app = FastAPI()

app.include_router(insurance_router)

connect_db(app)
