from .models import Tariff
from fastapi import HTTPException
import json


class InsuranceService:

    @staticmethod
    async def create_multy_tariff_by_date(tariffs_by_date: dict, is_file: bool = False):
        """Data sorted by date tariffs are received and created according to the cycle"""
        for date in tariffs_by_date:
            for tariff in tariffs_by_date[date]:
                tariff_date = {**tariff, "date": date}
                await InsuranceService.create_tariff(**tariff_date)
        return {"message": "tariffs created"}

    @staticmethod
    async def create_multy_tariff_json_file(contents: bytes):
        text_data = contents.decode("utf-8")
        try:
            data = json.loads(text_data)
        except json.JSONDecodeError as e:
            raise HTTPException(status_code=422, detail="Invalid JSON format in the file")
        result = await InsuranceService.create_multy_tariff_by_date(tariffs_by_date=data)
        return result

    @staticmethod
    async def create_tariff(**tariff_data):
        """Create a single tariff for a given date"""
        tariff = await Tariff.create(**tariff_data)
        return tariff

    @staticmethod
    async def calculate_insurance(data: dict):
        """calculates the cost of insurance at the current rate"""
        tariff = await Tariff.filter(date=data.get("date"), cargo_type=data.get("cargo_type")).first()
        if tariff:
            total_result = tariff.rate * data.get("declared_value", 0)
            return {"message": f"total_result {total_result}"}
        raise HTTPException(status_code=422, detail="Tariff not found")
