from tortoise import fields, models
from datetime import date


class Tariff(models.Model):
    """Tariff model need for orm and create table in db"""
    cargo_type = fields.CharField(max_length=100)
    rate = fields.DecimalField(max_digits=10, decimal_places=2)
    date = fields.DateField(auto_now_add=True, default=date.today)

    def __str__(self):
        return self.cargo_type
