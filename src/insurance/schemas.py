from pydantic import BaseModel
from decimal import Decimal
from typing import List, Dict
from datetime import date


class CreateTariffSchema(BaseModel):
    """Schema for create tariff"""
    cargo_type: str
    rate: Decimal


class ReadTariffSchema(CreateTariffSchema):
    """Schema for reed tariff"""
    id: int
    date: date


class CreateMultyTariffSchema(BaseModel):
    """Schema for create multiply tariff"""
    data: Dict[date, List[CreateTariffSchema]]


class CalculateSchema(BaseModel):
    """Schema for calculate insurance"""
    cargo_type: str
    declared_value: Decimal
    date: date
