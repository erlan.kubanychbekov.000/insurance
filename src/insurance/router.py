from fastapi import APIRouter, UploadFile
from .schemas import (
    CreateTariffSchema,
    ReadTariffSchema,
    CreateMultyTariffSchema,
    CalculateSchema
)
from .models import Tariff
from .service import InsuranceService
from typing import List

router = APIRouter(
    prefix="/insurance",
    tags=["Insurance"]
)


@router.post("/calculate/", status_code=200)
async def calculate_insurance(data: CalculateSchema) -> dict:
    """Calculation of the cost of insurance depending on the type of cargo and the declared value"""
    result = await InsuranceService.calculate_insurance(data.dict())
    return result


@router.post('/tariff/create/multy', status_code=201)
async def create_multy_tariff(data: CreateMultyTariffSchema) -> dict:
    """Create multy tariff by sorted date"""
    result = await InsuranceService.create_multy_tariff_by_date(tariffs_by_date=data.dict().get("data"))
    return result


@router.post("/upload/file/", status_code=201)
async def upload_file(file: UploadFile) -> dict:
    """Create multy tariff by upload file and sorted date"""
    contents = await file.read()
    result = await InsuranceService.create_multy_tariff_json_file(contents=contents)
    return result


@router.post("/tariff/create", response_model=ReadTariffSchema, status_code=201)
async def create_tariff(data: CreateTariffSchema) -> ReadTariffSchema:
    """Create tariff table"""
    tariff = await InsuranceService.create_tariff(**data.dict())
    return tariff


@router.get("/tariff/list/", status_code=200)
async def get_tariff_list() -> List[ReadTariffSchema]:
    """Get all tariff"""
    tariff_list = await Tariff.all().order_by("-date")
    return tariff_list
