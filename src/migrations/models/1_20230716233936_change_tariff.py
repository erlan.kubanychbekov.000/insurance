from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "tariff" ADD "rate" DECIMAL(10,2) NOT NULL;
        ALTER TABLE "tariff" DROP COLUMN "name";"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "tariff" ADD "name" VARCHAR(100) NOT NULL;
        ALTER TABLE "tariff" DROP COLUMN "rate";"""
