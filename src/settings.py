from decouple import config

TORTOISE_ORM = {
    "connections": {
        "default": config("DB_URL")
    },
    "apps": {
        "models": {
            "models": [
                "aerich.models",
                "src.insurance.models"
            ],
            "default_connection": "default",
        },
    },
}
