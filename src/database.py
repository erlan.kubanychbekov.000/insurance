from tortoise.contrib.fastapi import register_tortoise
from src.settings import TORTOISE_ORM


def connect_db(app):
    """Connect db function"""
    register_tortoise(
        app=app,
        config=TORTOISE_ORM,
        generate_schemas=True,
        add_exception_handlers=True,
    )
