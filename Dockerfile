
FROM python:3.8


ENV FASTAPI_ENV=production


COPY . /app
WORKDIR /app


RUN pip install --no-cache-dir -r requirements.txt


CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8000"]